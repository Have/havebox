﻿using HaveBox;
using HaveBox.SubConfigs;
using System.Diagnostics;
using System.Reflection;

namespace HaveBoxMeasurement
{
    public class Program
    {
        static void Main(string[] args)
        {
            var container = new Container();
            container.Configure(config => {
                config.MergeConfig(new SimpleScanner(Assembly.GetExecutingAssembly()));
                config.MergeConfig<ConfigInjection>();
                config.For<Stopwatch>().Use<Stopwatch>();
                config.For<IContainer>().Use<Container>();
            });
            container.GetInstance<IMeasureController>().Start();
        }
    }
}
