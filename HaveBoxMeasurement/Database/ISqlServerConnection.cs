﻿using System;
namespace HaveBoxMeasurement.Database
{
    public interface ISqlServerConnection
    {
        System.Data.SqlClient.SqlConnection Connection { get; }
    }
}
