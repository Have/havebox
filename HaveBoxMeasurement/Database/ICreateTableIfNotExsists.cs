﻿using System;
namespace HaveBoxMeasurement.Database
{
    public interface ICreateTableIfNotExsists
    {
        void Execute(string table, string createSql);
    }
}
