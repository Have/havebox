﻿using HaveBoxMeasurement.Dtos;

namespace HaveBoxMeasurement.SessionHandlers
{
    public interface ISessionHandler
    {
        void Handle(ISessionDto sessionDto);
    }
}
