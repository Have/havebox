﻿using HaveBoxMeasurement.Dtos;
using HaveBoxMeasurement.StatisticsHandlers;
using System;

namespace HaveBoxMeasurement.SessionHandlers
{
    public class ConsoleWriter : ISessionHandler
    {
        readonly IStatisticsHandler _statiticsHandler;

        public ConsoleWriter(IStatisticsHandler AvgConsoleWriter)
        {
            _statiticsHandler = AvgConsoleWriter;
        }

        public void Handle(ISessionDto sessionDto)
        {
            Console.Out.WriteLine("Session: {0}", sessionDto.SessionId);
            Console.Out.WriteLine("GitBranch: {0}", sessionDto.GitBranch);
            Console.Out.WriteLine("Timestamp: {0}", sessionDto.Timestamp);

            _statiticsHandler.Handle(sessionDto.SessionId, sessionDto.StatisticsDtos);
        }
    }
}
