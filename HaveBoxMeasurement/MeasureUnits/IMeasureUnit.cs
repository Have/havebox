﻿using HaveBoxMeasurement.Dtos;
using System.Collections.Generic;

namespace HaveBoxMeasurement.MeasureUnits
{
    public interface IMeasureUnit
    {
        IEnumerable<IStatisticsDto> Run();
    }
}
