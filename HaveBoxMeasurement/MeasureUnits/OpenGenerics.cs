﻿using HaveBox;
using System.Diagnostics;

namespace HaveBoxMeasurement.MeasureUnits
{
    public class OpenGenerics : MeasureUnit<OpenGenerics.ITransient<int>, OpenGenerics>, IMeasureUnit
    {
        public OpenGenerics(IKeyValueSet MeasureRuns, IKeyValueSet MeasureIterations, IContainer container, Stopwatch stopwatch)
            : base(MeasureRuns, MeasureIterations, container, stopwatch)
        {
        }

        public override void Configure(IContainer container)
        {
            container.Configure(config => config.For(typeof(ITransient<>)).Use(typeof(Transient<>)));
        }

        public interface ITransient<T>
        {
        }

        public class Transient<T> : ITransient<T>
        {
        }
    }
}
