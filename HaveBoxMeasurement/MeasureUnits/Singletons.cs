﻿using HaveBox;
using System.Diagnostics;

namespace HaveBoxMeasurement.MeasureUnits
{
    public class Singletons : MeasureUnit<Singletons.ISingleton, Singletons>, IMeasureUnit
    {
        public Singletons(IKeyValueSet MeasureRuns, IKeyValueSet MeasureIterations, IContainer container, Stopwatch stopwatch)
            : base(MeasureRuns, MeasureIterations, container, stopwatch)
        {

        }

        public override void Configure(IContainer container)
        {
            container.Configure(config => config.For<ISingleton>().Use<Singleton>().AsSingleton());
        }

        public interface ISingleton
        { 
        }

        public class Singleton : ISingleton
        {
        }
    }
}
