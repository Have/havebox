﻿
namespace HaveBoxMeasurement.Dtos
{
    public class StatisticsDto : IStatisticsDto
    {
        public string Name { get; set; }
        public long ElapsedMilliseconds { get; set; }
    }
}
