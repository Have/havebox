﻿using System;

namespace HaveBoxMeasurement.Dtos
{
    public interface IStatisticsDto
    {
        long ElapsedMilliseconds { get; }
        string Name { get; }
    }
}
