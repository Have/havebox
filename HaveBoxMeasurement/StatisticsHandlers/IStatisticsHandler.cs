﻿using HaveBoxMeasurement.Dtos;
using System;
using System.Collections.Generic;

namespace HaveBoxMeasurement.StatisticsHandlers
{
    public interface IStatisticsHandler
    {
        void Handle(Guid sessionId, IEnumerable<IStatisticsDto> statisticsDtos);
    }
}
