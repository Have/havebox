﻿using HaveBox;
using HaveBoxMeasurement.Dtos;
using System;
using System.Collections.Generic;

namespace HaveBoxMeasurement.StatisticsHandlers
{
    public class RawConsoleWriter : IStatisticsHandler
    {
        public void Handle(Guid sessionId, IEnumerable<IStatisticsDto> statisticsDtos)
        {
            statisticsDtos.Each(dto => Console.Out.WriteLine("{0} : {1}", dto.Name, dto.ElapsedMilliseconds));
        }
    }
}
